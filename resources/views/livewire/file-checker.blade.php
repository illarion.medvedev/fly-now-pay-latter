<div>
    <form wire:submit.prevent="saveForm">

    @if($showFormBlock)
    <div class="mb-10">
        <label for="content" class="block text-sm font-medium text-gray-700">1. Please paste your content here (Max 10Kb): OR
            <button type="button" wire:click="$emitUp('generateRandomString')" class="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Generate</button>
        </label>
        <div class="mt-1">
            <textarea wire:model="content" rows="4" value="{{ $content }}" name="content" id="content" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"></textarea>
        </div>
        @error('content')
            <div class="text-sm text-red-500 mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <fieldset class="space-y-5 mb-10">
        <legend class="text-sm font-medium text-gray-700">2. Check for:</legend>
        <div class="relative flex items-start">
            <div class="flex items-center h-5">
                <input wire:model="nonRepeating" id="nonRepeating" aria-describedby="comments-description" name="nonRepeating" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
            </div>
            <div class="ml-3 text-sm">
                <label for="nonRepeating" class="font-medium text-gray-700">non-repeating</label>
                <span id="nonRepeating-description" class="text-gray-500">tell me first non-repeating letter, punctuation or symbol</span>
            </div>
        </div>
        <div class="relative flex items-start">
            <div class="flex items-center h-5">
                <input wire:model="leastRepeating" id="leastRepeating" name="leastRepeating" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
            </div>
            <div class="ml-3 text-sm">
                <label for="leastRepeating" class="font-medium text-gray-700">least-repeating</label>
                <span id="candidates-description" class="text-gray-500">tell me first least-repeating letter, punctuation or symbol</span>
            </div>
        </div>
        <div class="relative flex items-start">
            <div class="flex items-center h-5">
                <input wire:model="mostRepeating" id="mostRepeating" name="mostRepeating" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
            </div>
            <div class="ml-3 text-sm">
                <label for="mostRepeating" class="font-medium text-gray-700">most-repeating</label>
                <span id="offers-description" class="text-gray-500">tell me first most-repeating letter, punctuation or symbol</span>
            </div>
        </div>
    </fieldset>

    <fieldset class="space-y-5 mb-10">
        <legend class="text-sm font-medium text-gray-700">2. Check should include:</legend>
        @foreach($characterFlags as $flag)
            <div class="relative flex items-start">
                <div class="flex items-center h-5">
                    <input id="{{$flag}}" name="{{$flag}}" value={{$flag}} wire:model="chosenCharacters" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                </div>
                <div class="ml-3 text-sm">
                    <label for="{{$flag}}" class="font-medium text-gray-700">{{$flag}}</label>
                </div>
            </div>
        @endforeach
    </fieldset>

    <div class="flex justify-center items-center">
        <button type="submit" {{ ($isFormSubmitted) ? 'disabled' : ''}} class="inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white {{ (!$isFormSubmitted) ? 'bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500' : 'bg-gray-300' }}">
            Submit
            <!-- Heroicon name: solid/mail -->
            <svg class="ml-2 -mr-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
            </svg>
        </button>
    </div>
    @endif
    </form>
    @if($showResultsBlock)
        <div>
            <h3 class="mb-6 text-lg leading-6 font-medium text-gray-900">
                Process completed
            </h3>
            <div class="mb-6">
                File size is: <span class="bg-yellow-100 text-yellow-800">{{ number_format($fileSize, 2, '.', '') }} Bytes</span>
            </div>
            <div class="mb-6">
                Execution length in seconds: <span class="bg-blue-100 text-blue-800">{{ $execTime }}</span>
            </div>
            <div class="mb-6">
                <div class="mb-2">Program output:</div>
                <div>
                    @foreach($commandOutput as $result)
                        @if(is_object($result) && property_exists($result,'type') && property_exists($result,'message'))
                            <div class="mb-2 items-center px-5 py-2 rounded-full text-sm font-medium @if($result->type == 'success') bg-green-100 text-green-800 @else bg-red-100 text-red-800 @endif">
                                {{ $result->message }}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="mb-10" wire:ignore>
                <div x-data="{
                                    id: 1,
                                    get expanded() {
                                        return this.active === this.id
                                    },
                                    set expanded(value) {
                                        this.active = value ? this.id : null
                                    },
                                }" role="region" class="bg-white rounded-lg shadow">
                    <h2>
                        <button
                            x-on:click="expanded = !expanded"
                            :aria-expanded="expanded"
                            class="flex items-center justify-between w-full font-bold px-6 py-4"
                        >
                            <span>View submitted content</span>
                            <span x-show="expanded" aria-hidden="true" class="ml-4">&minus;</span>
                            <span x-show="!expanded" aria-hidden="true" class="ml-4">&plus;</span>
                        </button>
                    </h2>

                    <div x-show="expanded" x-collapse>
                        <div class="pb-4 px-6">
                            {{ $content }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-center items-center">
                <button wire:click="$emitUp('backToForm')" type="button" class="inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    <svg class="mr-2 -ml-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" />
                    </svg>
                    Back
                </button>
            </div>
        </div>
    @endif
</div>
