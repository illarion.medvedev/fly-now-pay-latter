<?php

namespace Tests\Unit;

use App\Http\Livewire\FileChecker;
use PHPUnit\Framework\TestCase;

class FileCheckerCoreTest extends TestCase
{

    public $testArray = [
        'c' => array(
            'count' => 2,
            'type' => 'include-letter',
            'index' => 0,
            'character' => 'c'
        ),
        'a' => array(
            'count' => 1,
            'type' => 'include-letter',
            'index' => 1,
            'character' => 'a'
        ),
        '@' => array(
            'count' => 4,
            'type' => 'include-punctuation',
            'index' => 2,
            'character' => '@'
        ),
        'b' => array(
            'count' => 1,
            'type' => 'include-letter',
            'index' => 3,
            'character' => 'b'
        ),
        ',' => array(
            'count' => 1,
            'type' => 'include-punctuation',
            'index' => 4,
            'character' => ','
        ),
        '"' => array(
            'count' => 2,
            'type' => 'include-punctuation',
            'index' => 5,
            'character' => '"'
        ),
        'h' => array(
            'count' => 4,
            'type' => 'include-letter',
            'index' => 6,
            'character' => 'h'
        ),
        '.' => array(
            'count' => 1,
            'type' => 'include-punctuation',
            'index' => 7,
            'character' => '.'
        ),
        '*' => array(
            'count' => 3,
            'type' => 'include-punctuation',
            'index' => 8,
            'character' => '*'
        ),
        '%' => array(
            'count' => 1,
            'type' => 'include-symbol',
            'index' => 9,
            'character' => '%'
        ),
        '^' => array(
            'count' => 4,
            'type' => 'include-symbol',
            'index' => 10,
            'character' => '^'
        ),
        ')' => array(
            'count' => 1,
            'type' => 'include-punctuation',
            'index' => 11,
            'character' => ')'
        )
    ];

    public function test_check_character_function_with_first_character()
    {
        $FileChecker = new FileChecker();
        $FileChecker->checkCharacter('a', 0);
        $this->assertTrue($FileChecker->processArray['a'] != []);
    }

    public function test_check_character_function_with_two_characters()
    {
        $FileChecker = new FileChecker();
        $FileChecker->checkCharacter('a', 0);
        $FileChecker->checkCharacter('a', 5);
        $this->assertTrue($FileChecker->processArray['a']['count'] == 2);
    }

    public function test_detect_character_type_with_letter()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->detectCharacterType('h') == 'include-letter');
    }

    public function test_detect_character_type_with_punctuation()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->detectCharacterType(':') == 'include-punctuation');
    }

    public function test_detect_character_type_with_symbol()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->detectCharacterType('^') == 'include-symbol');
    }

    public function test_first_non_repeating_character_with_letter()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First non-repeating letter is: a';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstNonRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_non_repeating_character_with_punctuation()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First non-repeating punctuation is: ,';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstNonRepeatingCharacter('include-punctuation');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_non_repeating_character_with_symbol()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First non-repeating symbol is: %';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstNonRepeatingCharacter('include-symbol');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_non_repeating_character_with_error()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = [
            '^' => array(
                'count' => 4,
                'type' => 'include-symbol',
                'index' => 10,
                'character' => '^'
            ),
            ')' => array(
                'count' => 1,
                'type' => 'include-punctuation',
                'index' => 11,
                'character' => ')'
            )
        ];
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First non-repeating letter is: None';
        $expectedObj->type = 'error';
        $resultObject = $FileChecker->getFirstNonRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_least_repeating_character_with_letter()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First least repeating letter is: a';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstLeastRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_least_repeating_character_with_punctuation()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First least repeating punctuation is: ,';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstLeastRepeatingCharacter('include-punctuation');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_least_repeating_character_with_symbol()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First least repeating symbol is: %';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstLeastRepeatingCharacter('include-symbol');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_least_repeating_character_with_error()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = [
            '^' => array(
                'count' => 4,
                'type' => 'include-symbol',
                'index' => 10,
                'character' => '^'
            ),
            ')' => array(
                'count' => 1,
                'type' => 'include-punctuation',
                'index' => 11,
                'character' => ')'
            )
        ];
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First least-repeating letter is: None';
        $expectedObj->type = 'error';
        $resultObject = $FileChecker->getFirstLeastRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_most_repeating_character_with_letter()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First most repeating letter is: h';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstMostRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_most_repeating_character_with_punctuation()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First most repeating punctuation is: @';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstMostRepeatingCharacter('include-punctuation');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_most_repeating_character_with_symbol()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = $this->testArray;
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First most repeating symbol is: ^';
        $expectedObj->type = 'success';
        $resultObject = $FileChecker->getFirstMostRepeatingCharacter('include-symbol');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_first_most_repeating_character_with_error()
    {
        $FileChecker = new FileChecker();
        $FileChecker->processArray = [
            '^' => array(
                'count' => 4,
                'type' => 'include-symbol',
                'index' => 10,
                'character' => '^'
            ),
            ')' => array(
                'count' => 1,
                'type' => 'include-punctuation',
                'index' => 11,
                'character' => ')'
            )
        ];
        $expectedObj = new \stdClass();
        $expectedObj->message = 'First most repeating letter is: None';
        $expectedObj->type = 'error';
        $resultObject = $FileChecker->getFirstMostRepeatingCharacter('include-letter');
        $this->assertTrue(
            is_object(
                $resultObject
            ) && $resultObject->message == $expectedObj->message && $resultObject->type == $expectedObj->type
        );
    }

    public function test_content_is_empty_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->content == '');
    }

    public function test_checking_scenarios_are_not_chosen_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue(
            $FileChecker->nonRepeating == false && $FileChecker->mostRepeating == false && $FileChecker->leastRepeating == false
        );
    }

    public function test_characters_flags_are_presenting()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue(
            $FileChecker->characterFlags != []
        );
    }

    public function test_chosen_characters_are_empty_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue(
            $FileChecker->chosenCharacters == []
        );
    }

    public function test_regex()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue(
            $FileChecker->regex == '/^[a-z!@$|#^~{}<>%§_=+\-()[`{{ordinary}}"\]:;.,\\\&*\/?]{1,}$/'
        );
    }
}
