<?php

namespace Tests\Unit;

use App\Http\Livewire\FileChecker;
use PHPUnit\Framework\TestCase;

class FileCheckerDesktopTest extends TestCase
{
    public function test_form_is_not_submitted_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->isFormSubmitted == false);
    }

    public function test_form_block_is_shown_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->showFormBlock == true);
    }

    public function test_result_block_is_hidden_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->showResultsBlock == false);
    }

    public function test_command_output_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->commandOutput == []);
    }

    public function test_execution_time_by_default()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->execTime == 0);
    }

    public function test_validation_rules_not_empty()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->rules != []);
    }

    public function test_back_to_form_button()
    {
        $FileChecker = new FileChecker();
        $FileChecker->backToForm();
        $this->assertTrue(
            $FileChecker->showResultsBlock == false && $FileChecker->showFormBlock == true && $FileChecker->isFormSubmitted == false
        );
    }

    public function test_checker_empty_content()
    {
        $FileChecker = new FileChecker();
        $this->assertTrue($FileChecker->contentChecker() == 'Program exited with error code: 1');
    }

    public function test_checker_file_size_too_big()
    {
        $FileChecker = new FileChecker();
        $FileChecker->content = 'abc';
        $FileChecker->fileSize = 10001;
        $this->assertTrue($FileChecker->contentChecker() == 'Program exited with error. Text is too large');
    }

    public function test_checker_empty_chosen_characters()
    {
        $FileChecker = new FileChecker();
        $FileChecker->content = 'abc';
        $FileChecker->fileSize = 800;
        $this->assertTrue($FileChecker->contentChecker() == 'Program exited with error code: 4');
    }

    public function test_checker_empty_check_scenarios()
    {
        $FileChecker = new FileChecker();
        $FileChecker->content = 'abc';
        $FileChecker->fileSize = 800;
        $FileChecker->chosenCharacters = ['include-letter'];
        $this->assertTrue($FileChecker->contentChecker() == 'Program exited with error code: 3');
    }

    public function test_checker_regex()
    {
        $FileChecker = new FileChecker();
        $FileChecker->content = 'AC ';
        $FileChecker->fileSize = 800;
        $FileChecker->chosenCharacters = ['include-letter'];
        $FileChecker->nonRepeating = true;
        $this->assertTrue($FileChecker->contentChecker() == 'Program exited with error code: 2.');
    }

    public function test_finish_process()
    {
        $FileChecker = new FileChecker();
        $FileChecker->finishProcess();
        $this->assertTrue($FileChecker->isFormSubmitted == false);
    }

    public function test_generate_random_string()
    {
        $FileChecker = new FileChecker();
        $testString = $FileChecker->generateRandomString(100);
        $this->assertTrue($FileChecker->content == $testString);
    }

    public function test_log_command_output()
    {
        $FileChecker = new FileChecker();
        $result = $FileChecker->logCommandOutput('test message');
        $this->assertTrue(is_object($result) && $result->message == 'test message' && $result->type == 'error');
    }
}
