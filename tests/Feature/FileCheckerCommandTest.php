<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FileCheckerCommandTest extends TestCase
{
    public function test_command_without_any_options()
    {
        $this->artisan('file-checker:run')
            ->expectsOutput('Program exited with error code: 1')
            ->assertExitCode(1);
    }

    public function test_command_with_incorrect_file_path()
    {
        $this->artisan('file-checker:run -i fake.txt')
            ->expectsOutput('Program exited with error code: 1')
            ->assertExitCode(1);
    }

    public function test_command_with_correct_format()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating')
            ->expectsOutput('Program exited with error code: 4')
            ->assertExitCode(4);
    }

    public function test_command_with_incorrect_format()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating2')
            ->expectsOutput('Program exited with error code: 3')
            ->assertExitCode(3);
    }

    public function test_command_with_failed_regex()
    {
        $this->artisan('file-checker:run -i fail_test.txt -f most-repeating')
            ->expectsOutput('Program exited with error code: 2')
            ->assertExitCode(2);
    }

    public function test_command_with_correct_format_and_characters()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating -L')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First most repeating letter is: r')
            ->assertExitCode(200);
    }

    public function test_command_with_most_repeating_punctuation()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating -P')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First most repeating punctuation is: (')
            ->assertExitCode(200);
    }

    public function test_command_with_most_repeating_symbol()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First most repeating symbol is: -')
            ->assertExitCode(200);
    }

    public function test_command_with_non_repeating_letter()
    {
        $this->artisan('file-checker:run -i test.txt -f non-repeating -L')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First non-repeating letter is: None')
            ->assertExitCode(200);
    }

    public function test_command_with_non_repeating_punctuation()
    {
        $this->artisan('file-checker:run -i test.txt -f non-repeating -P')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First non-repeating punctuation is: None')
            ->assertExitCode(200);
    }

    public function test_command_with_non_repeating_symbol()
    {
        $this->artisan('file-checker:run -i test.txt -f non-repeating -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First non-repeating symbol is: None')
            ->assertExitCode(200);
    }

    public function test_command_with_least_repeating_letter()
    {
        $this->artisan('file-checker:run -i test.txt -f least-repeating -L')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First least repeating letter is: j')
            ->assertExitCode(200);
    }

    public function test_command_with_least_repeating_punctuation()
    {
        $this->artisan('file-checker:run -i test.txt -f least-repeating -P')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First least repeating punctuation is: ?')
            ->assertExitCode(200);
    }

    public function test_command_with_least_repeating_symbol()
    {
        $this->artisan('file-checker:run -i test.txt -f least-repeating -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First least repeating symbol is: #')
            ->assertExitCode(200);
    }

    public function test_command_with_most_repeating_all_scenarios()
    {
        $this->artisan('file-checker:run -i test.txt -f most-repeating -L -P -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First most repeating letter is: r')
            ->expectsOutput('First most repeating punctuation is: (')
            ->expectsOutput('First most repeating symbol is: -')
            ->assertExitCode(200);
    }

    public function test_command_with_non_repeating_all_scenarios()
    {
        $this->artisan('file-checker:run -i test.txt -f non-repeating -L -P -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First non-repeating letter is: None')
            ->expectsOutput('First non-repeating punctuation is: None')
            ->expectsOutput('First non-repeating symbol is: None')
            ->assertExitCode(200);
    }

    public function test_command_with_least_repeating_all_scenarios()
    {
        $this->artisan('file-checker:run -i test.txt -f least-repeating -L -P -S')
            ->expectsOutput('File: test.txt')
            ->expectsOutput('First least repeating letter is: j')
            ->expectsOutput('First least repeating punctuation is: ?')
            ->expectsOutput('First least repeating symbol is: #')
            ->assertExitCode(200);
    }
}
