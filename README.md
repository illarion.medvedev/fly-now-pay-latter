Hi, I've done file content checker functionallity in both ways (as web form & as console command)
<br/> Thanks, was fun to develop

Tech stack: TALL (<a href="https://tailwindcss.com/" target="_blank">TailwindCSS</a> <a href="https://alpinejs.dev/" target="_blank">AlpineJS</a> <a href="https://laravel-livewire.com/" target="_blank">Livewire</a> Laravel)

Files with changes:
- PHP:
```
app/Console/Commands/FileChecker.php
app/Http/Livewire/FileChecker.php
app/Models/FileChecker.php
```
- View:
```
resources/views/livewire/file-checker.blade.php
```
- Tests:
```
tests/Feature/FileCheckerCommandTest.php
tests/Unit/FileCheckerCoreTest.php
tests/Unit/FileCheckerDesktopTest.php
```

Run tests: ```php artisan test```

Run command (example): ```php artisan file-checker:run -i test.txt -f least-repeating -L -P -S```
*files for tests are included

Web form is located on root page:    ```'/'```
