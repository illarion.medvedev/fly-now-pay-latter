<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FileChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file-checker:run {--i|input=} {--f|format=} {--L|include-letter} {--P|include-punctuation} {--S|include-symbol}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fly now - pay later test assignment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function logCommandOutput(object $obj): string
    {
        if ($obj->type == 'success') {
            $this->info($obj->message);
            return $obj->message;
        } else {
            $this->error($obj->message);
            return $obj->message;
        }
    }

    public function handle()
    {
        if ($this->option('input')) {
            try {
                $content = file_get_contents($this->option('input'));
            } catch (\Exception $error) {
                $this->error('Program exited with error code: 1');
                return 1;
            }
            if ($content != '') {
                $fileChecker = new \App\Models\FileChecker();
                $fileChecker->fileSize = strlen($content);
                if ($this->option('include-letter')) {
                    array_push($fileChecker->chosenCharacters, 'include-letter');
                }
                if ($this->option('include-punctuation')) {
                    array_push($fileChecker->chosenCharacters, 'include-punctuation');
                }
                if ($this->option('include-symbol')) {
                    array_push($fileChecker->chosenCharacters, 'include-symbol');
                }
                if ($this->option('format') && in_array($this->option('format'), $fileChecker->checkFlags)) {
                    if (preg_match(str_replace("{{ordinary}}", "'", $fileChecker->regex), $content)) {
                        for ($i = 0; $i < $fileChecker->fileSize; $i++) {
                            $fileChecker->checkCharacter($content[$i], $i);
                        }
                        if ($fileChecker->chosenCharacters != []) {
                            $this->info('File: ' . $this->option('input'));
                        } else {
                            $this->error('Program exited with error code: 4');
                            return 4;
                        }
                        switch ($this->option('format')) {
                            case 'non-repeating':
                                foreach ($fileChecker->chosenCharacters as $characterType) {
                                    $response = $fileChecker->getFirstNonRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response);
                                }
                                break;
                            case 'least-repeating':
                                foreach ($fileChecker->chosenCharacters as $characterType) {
                                    $response = $fileChecker->getFirstLeastRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response);
                                }
                                break;
                            case 'most-repeating':
                                foreach ($fileChecker->chosenCharacters as $characterType) {
                                    $response = $fileChecker->getFirstMostRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response);
                                }
                                break;
                        }
                    } else {
                        $this->error('Program exited with error code: 2');
                        return 2;
                    }
                } else {
                    $this->error('Program exited with error code: 3');
                    return 3;
                }
            }
        } else {
            $this->error('Program exited with error code: 1');
            return 1;
        }
        return 200;
    }
}
