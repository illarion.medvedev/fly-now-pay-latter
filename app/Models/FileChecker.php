<?php

namespace App\Models;

use Livewire\Component;
use stdClass;

class FileChecker extends Component
{
    public $content = '';

    public $processArray = [];

    public $nonRepeating = false;
    public $leastRepeating = false;
    public $mostRepeating = false;

    public $fileSize = 0;

    public $checkFlags = ['non-repeating', 'least-repeating', 'most-repeating'];
    public $characterFlags = ['include-letter', 'include-punctuation', 'include-symbol'];
    public $chosenCharacters = [];

    public $regex = '/^[a-z!@$|#^~{}<>%§_=+\-()[`{{ordinary}}"\]:;.,\\\&*\/?]{1,}$/';

    public function checkCharacter(string $character, int $index)
    {
        if (isset($this->processArray[$character])) {
            $this->processArray[$character]['count'] += 1;
        } else {
            $this->processArray[$character] = array(
                'count' => 1,
                'type' => $this->detectCharacterType($character),
                'index' => $index,
                'character' => $character
            );
        }
    }

    public function detectCharacterType(string $character): string
    {
        if (preg_match('/[a-z]/', $character)) {
            return 'include-letter';
        } elseif (preg_match(str_replace("{{ordinary}}", "'", '/[.,?;!:(){{ordinary}}[\]\/-@{}*`"]/'), $character)) {
            return 'include-punctuation';
        } else {
            return 'include-symbol';
        }
    }

    public function getFirstNonRepeatingCharacter(string $type): stdClass
    {
        $filtered = array_filter($this->processArray, function ($v, $k) use ($type) {
            return $v['count'] == 1 && $v['type'] == $type;
        },                       ARRAY_FILTER_USE_BOTH);
        $assertableType = str_replace('include-', '', $type);
        usort($filtered, function ($a, $b) {
            return $a['index'] <=> $b['index'];
        });
        $returnObj = new StdClass;
        if ($filtered == []) {
            $returnObj->message = 'First non-repeating ' . $assertableType . ' is: None';
            $returnObj->type = 'error';
        } else {
            $returnObj->message = 'First non-repeating ' . $assertableType . ' is: ' . $filtered[0]['character'];
            $returnObj->type = 'success';
        }
        return $returnObj;
    }

    public function getFirstLeastRepeatingCharacter(string $type): stdClass
    {
        $filtered = array_filter($this->processArray, function ($v, $k) use ($type) {
            return $v['count'] >= 1 && $v['type'] == $type;
        },                       ARRAY_FILTER_USE_BOTH);
        $assertableType = str_replace('include-', '', $type);
        $returnObj = new StdClass;
        if ($filtered == []) {
            $returnObj->message = 'First least-repeating ' . $assertableType . ' is: None';
            $returnObj->type = 'error';
        } else {
            $countColumn = array_column($filtered, 'count');
            $minCount = min($countColumn);
            $filtered = array_filter($filtered, function ($v, $k) use ($minCount) {
                return $v['count'] == $minCount;
            },                       ARRAY_FILTER_USE_BOTH);
            usort($filtered, function ($a, $b) {
                return $a['index'] <=> $b['index'];
            });
            $returnObj->message = 'First least repeating ' . $assertableType . ' is: ' . $filtered[0]['character'];
            $returnObj->type = 'success';
        }
        return $returnObj;
    }

    public function getFirstMostRepeatingCharacter(string $type): stdClass
    {
        $filtered = array_filter($this->processArray, function ($v, $k) use ($type) {
            return $v['type'] == $type;
        },                       ARRAY_FILTER_USE_BOTH);
        $assertableType = str_replace('include-', '', $type);
        arsort($filtered);
        $returnObj = new StdClass;
        if ($filtered == []) {
            $returnObj->message = 'First most repeating ' . $assertableType . ' is: None';
            $returnObj->type = 'error';
        } else {
            $countColumn = array_column($filtered, 'count');
            $maxCount = max($countColumn);
            $filtered = array_filter($filtered, function ($v, $k) use ($maxCount) {
                return $v['count'] == $maxCount;
            },                       ARRAY_FILTER_USE_BOTH);
            usort($filtered, function ($a, $b) {
                return $a['index'] <=> $b['index'];
            });
            $returnObj->message = 'First most repeating ' . $assertableType . ' is: ' . $filtered[0]['character'];
            $returnObj->type = 'success';
        }
        return $returnObj;
    }
}
