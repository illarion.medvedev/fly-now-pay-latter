<?php

namespace App\Http\Livewire;

use Livewire\Component;
use stdClass;

class FileChecker extends \App\Models\FileChecker
{
    public $isFormSubmitted = false;

    public $showResultsBlock = false;
    public $showFormBlock = true;

    public $commandOutput = [];
    public $execTime = 0;

    public $rules = [
        'content' => 'required|min:1'
    ];

    public function __construct()
    {
    }

    protected $listeners = ['generateRandomString' => 'generateRandomString', 'backToForm' => 'backToForm'];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function backToForm()
    {
        $this->showResultsBlock = false;
        $this->showFormBlock = true;
        $this->isFormSubmitted = false;
    }

    public function saveForm()
    {
        $this->validate();
        $this->fileSize = strlen($this->content);
        $this->showFormBlock = false;
        $this->showResultsBlock = true;
        $this->isFormSubmitted = true;
        $time_pre = microtime(true);
        $this->contentChecker();
        $time_post = microtime(true);
        $this->execTime = $time_post - $time_pre;
    }

    public function contentChecker()
    {
        if ($this->content != '') {
            if ($this->fileSize <= 10000) {
                if ($this->chosenCharacters != []) {
                    if ($this->nonRepeating || $this->leastRepeating || $this->mostRepeating) {
                        if (preg_match(str_replace("{{ordinary}}", "'", $this->regex), $this->content)) {
                            for ($i = 0; $i < $this->fileSize; $i++) {
                                $this->checkCharacter($this->content[$i], $i);
                            }
                            if ($this->nonRepeating) {
                                foreach ($this->chosenCharacters as $characterType) {
                                    $response = $this->getFirstNonRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response->message, $response->type);
                                }
                            }
                            if ($this->leastRepeating) {
                                foreach ($this->chosenCharacters as $characterType) {
                                    $response = $this->getFirstLeastRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response->message, $response->type);
                                }
                            }
                            if ($this->mostRepeating) {
                                foreach ($this->chosenCharacters as $characterType) {
                                    $response = $this->getFirstMostRepeatingCharacter($characterType);
                                    $this->logCommandOutput($response->message, $response->type);
                                }
                            }
                        } else {
                            $this->logCommandOutput('Program exited with error code: 2.');
                            return 'Program exited with error code: 2.';
                        }
                    } else {
                        $this->logCommandOutput('Program exited with error code: 3');
                        return 'Program exited with error code: 3';
                    }
                } else {
                    $this->logCommandOutput('Program exited with error code: 4');
                    return 'Program exited with error code: 4';
                }
            } else {
                $this->logCommandOutput('Program exited with error. Text is too large');
                return 'Program exited with error. Text is too large';
            }
        } else {
            $this->logCommandOutput('Program exited with error code: 1');
            return 'Program exited with error code: 1';
        }
        $this->finishProcess();
    }

    public function finishProcess()
    {
        $this->isFormSubmitted = false;
        $this->processArray = [];
    }

    public function generateRandomString(int $n = 1000): string
    {
        $allowedCharacters = 'abcdefghijklmnopqrstuvwxyz!@$|#^~{}<>%_=+-()[]`":;.,&*?\/';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($allowedCharacters) - 1);
            $randomString .= $allowedCharacters[$index];
        }
        $this->content = $randomString;
        return $randomString;
    }

    public function logCommandOutput(string $message, string $type = 'error'): stdClass
    {
        $obj = new stdClass();
        $obj->message = $message;
        $obj->type = $type;
        $this->commandOutput[] = $obj;
        return $obj;
    }

    public function render()
    {
        return view('livewire.file-checker');
    }
}
